// Karma configuration
// http://karma-runner.github.io/0.12/config/configuration-file.html
// Generated on 2015-02-09 using
// generator-karma 0.9.0

module.exports = function(config) {
  'use strict';

  config.set({
    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // base path, that will be used to resolve files and exclude
    basePath: '../',

    // testing framework to use (jasmine/mocha/qunit/...)
    frameworks: ['jasmine'],

    reporters: ['progress', 'coverage'],

    preprocessors: {
      // source files, that you wanna generate coverage for
      // do not include tests or libraries
      // (these files will be instrumented by Istanbul)
      'app/scripts/**/*.js': 'coverage'
    },

    coverageReporter: {
      type : 'html',
      dir : 'coverage/'
    },

    // list of files / patterns to load in the browser
    files: [
      // bower:js
      'app/components/modernizr/modernizr.js',
      'app/components/jquery/dist/jquery.js',
      'app/components/angular/angular.js',
      'app/components/angular-animate/angular-animate.js',
      'app/components/angular-cookies/angular-cookies.js',
      'app/components/angular-resource/angular-resource.js',
      'app/components/angular-route/angular-route.js',
      'app/components/angular-sanitize/angular-sanitize.js',
      'app/components/angular-touch/angular-touch.js',
      'app/components/fastclick/lib/fastclick.js',
      'app/components/jquery.cookie/jquery.cookie.js',
      'app/components/jquery-placeholder/jquery.placeholder.js',
      'app/components/foundation/js/foundation.js',
      'app/components/foundation/js/foundation/foundation.tooltip.js',
      'app/components/foundation/js/foundation/foundation.reveal.js',
      'app/components/angular-local-storage/dist/angular-local-storage.js',
      'app/components/angular-toastr/dist/angular-toastr.js',
      'app/components/angular-filter/dist/angular-filter.js',
      'app/components/angularjs-datepicker/dist/angular-datepicker.min.js',
      'app/components/Chart.js/Chart.min.js',
      'app/components/tc-angular-chartjs/dist/tc-angular-chartjs.min.js',
      'app/components/angular-mocks/angular-mocks.js',
      // endbower
      'app/customComponents/**/*.js',
      'app/scripts/**/*.js',
      'test/mock/**/*.js',
      'test/spec/**/*.js'
    ],

    // list of files / patterns to exclude
    exclude: [
    ],

    // web server port
    port: 8080,

    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: [
      'PhantomJS'
    ],

    // Which plugins to enable
    plugins: [
      'karma-phantomjs-launcher',
      'karma-jasmine',
      'karma-coverage',
    ],

    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false,

    colors: true,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO

    // Uncomment the following lines if you are using grunt's server to run the tests
    // proxies: {
    //   '/': 'http://localhost:9000/'
    // },
    // URL root prevent conflicts with the site root
    // urlRoot: '_karma_'
  });
};
