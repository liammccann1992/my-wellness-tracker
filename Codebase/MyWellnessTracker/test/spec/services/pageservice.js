'use strict';

describe('Service: pageService', function () {

  // load the service's module
  beforeEach(module('myWellnessTrackerApp'));

  var $httpBackend,pageInfoFileHandler,mocked,pageService;

  mocked =  [{
    "name" : "sideeffects",
    "popupTitle" : "pt1Title",
    "popupText" : "pt1Text"
  },
  {
    "name"  : "addsideeffect",
    "popupTitle" : "pt2Title",
    "popupText" : "pt2Text"
  }];

  beforeEach(inject(function($injector,_pageService_) {
    // Set up the mock http service responses
    $httpBackend = $injector.get('$httpBackend');
    // backend definition common for all tests
    pageInfoFileHandler = $httpBackend.when('GET', 'data/pageInformation.json')
      .respond(mocked);

    pageService = _pageService_;
  }));

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('should fetch page information json', function() {
    $httpBackend.expectGET('data/pageInformation.json');

    var returnValue = pageService.getPageInformation("sideeffects").then(function(res){
      expect(res.name).toEqual("sideeffects");
      expect(res.popupTitle).toEqual("pt1Title");
      expect(res.popupText).toEqual("pt1Text");

    });


    $httpBackend.flush();
  });

  it('should return no page found', function() {
    $httpBackend.expectGET('data/pageInformation.json');

    var returnValue = pageService.getPageInformation("notarealpage").then(function(res){
      expect(res).toEqual("No Page found");
    });


    $httpBackend.flush();
  });

});
