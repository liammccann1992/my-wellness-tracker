'use strict';

describe('Service: permissionsService', function () {

  // load the service's module
  beforeEach(module('myWellnessTrackerApp'));


  var permissionsService;
  var localStorageServiceMock;

  describe('Permission service initialised with empty local storage (similar to first time starting the app)', function(){
    var localStorageService_TermObject = {};

    beforeEach(function() {

      localStorageServiceMock = {
        obj : localStorageService_TermObject,

        get: function() {
          return this.obj;
        },
        set : function(key,value){
          this.obj = value;
        }
      };

      module('myWellnessTrackerApp',function($provide) {
        $provide.value('localStorageService', localStorageServiceMock);
      });

      inject(function (_permissionsService_) {
        permissionsService = _permissionsService_;
      });

    });

    it('get method should return empty object', function () {
      console.log(permissionsService.get());
      expect(permissionsService.get()).toBe(localStorageService_TermObject);
    });


    it('terms and conditions check should return false', function(){
      expect(permissionsService.termsAndConditionsComplete()).toBe(false);
    });

    it('disclaimer complete check should return false', function(){
      expect(permissionsService.disclaimerComplete()).toBe(false);
    });

    it('disclaimer should set to true', function(){
      permissionsService.setDisclaimerComplete(true);
      expect(permissionsService.disclaimerComplete()).toBe(true);
    });

    it('terms and conditions should set to true', function(){
      permissionsService.setTermsAndConditionsComplete(true);
      expect(permissionsService.termsAndConditionsComplete()).toBe(true);
    });

    it('disclaimer should set to false', function(){
      permissionsService.setDisclaimerComplete(false);
      expect(permissionsService.disclaimerComplete()).toBe(false);
    });

    it('terms and conditions should set to false', function(){
      permissionsService.setTermsAndConditionsComplete(false);
      expect(permissionsService.termsAndConditionsComplete()).toBe(false);
    });
  });

  describe('All permissions granted', function () {
    var localStorageService_TermObject = {
      "termsAndConditions": {
        "agreed": true,
        "when": 1425393320.402
      },
      "disclaimer": {
        "agreed": true,
        "when": 1425393440.896
      }
    };

    beforeEach(function() {
      localStorageServiceMock = {
        obj : localStorageService_TermObject,

        get: function() {
          return this.obj;
        },
        set : function(key,value){
          this.obj = value;
        }
      };

      module('myWellnessTrackerApp',function($provide) {
        $provide.value('localStorageService', localStorageServiceMock);
      });

      inject(function (_permissionsService_) {
        permissionsService = _permissionsService_;
      });

    });

    it('get method should return list of values provided by local storage servicet', function () {
      expect(permissionsService.get()).toBe(localStorageService_TermObject);
    });


    it('terms and conditions check should return true', function(){
      expect(permissionsService.termsAndConditionsComplete()).toBe(true);
    });

    it('disclaimer complete check should return true', function(){
      expect(permissionsService.disclaimerComplete()).toBe(true);
    });

    it('disclaimer should set to true', function(){
      permissionsService.setDisclaimerComplete(true);
      expect(permissionsService.disclaimerComplete()).toBe(true);
    });

    it('terms and conditions should set to true', function(){
      permissionsService.setTermsAndConditionsComplete(true);
      expect(permissionsService.termsAndConditionsComplete()).toBe(true);
    });

    it('disclaimer should set to false', function(){
      permissionsService.setDisclaimerComplete(false);
      expect(permissionsService.disclaimerComplete()).toBe(false);
    });

    it('terms and conditions should set to false', function(){
      permissionsService.setTermsAndConditionsComplete(false);
      expect(permissionsService.termsAndConditionsComplete()).toBe(false);
    });

  });







});
