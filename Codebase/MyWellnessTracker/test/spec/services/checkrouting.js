'use strict';

describe('Service: checkRouting', function () {

  // load the service's module
  beforeEach(module('myWellnessTrackerApp'));

  // instantiate service
  var checkRouting;
  beforeEach(inject(function (_checkRouting_) {
    checkRouting = _checkRouting_;
  }));

  it('should do something', function () {
    expect(!!checkRouting).toBe(true);
  });

});
