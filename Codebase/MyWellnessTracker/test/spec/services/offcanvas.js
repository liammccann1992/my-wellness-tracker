'use strict';

describe('Service: offCanvas', function () {

  // load the service's module
  beforeEach(module('myWellnessTrackerApp'));

  // instantiate service
  var offCanvas;
  beforeEach(inject(function (_offCanvas_) {
    offCanvas = _offCanvas_;
  }));

  it('should do something', function () {
    expect(!!offCanvas).toBe(true);
  });

});
