'use strict';

describe('Controller: ViewsideeffectsCtrl', function () {

  // load the controller's module
  beforeEach(module('myWellnessTrackerApp'));

  var ViewsideeffectsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ViewsideeffectsCtrl = $controller('ViewsideeffectsCtrl', {
      $scope: scope
    });
  }));

  //it('should have the correct title', function () {
  //  expect(scope.title).toBe("View Recorded Side Effects");
  //});
});
