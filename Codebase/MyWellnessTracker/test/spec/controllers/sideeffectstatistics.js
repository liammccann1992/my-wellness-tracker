'use strict';

describe('Controller: SideeffectstatisticsCtrl', function () {

  // load the controller's module
  beforeEach(module('myWellnessTrackerApp'));

  var SideeffectstatisticsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SideeffectstatisticsCtrl = $controller('SideeffectstatisticsCtrl', {
      $scope: scope
    });
  }));

  //it('should attach a list of awesomeThings to the scope', function () {
  //  expect(scope.awesomeThings.length).toBe(3);
  //});
});
