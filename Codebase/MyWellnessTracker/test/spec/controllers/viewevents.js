'use strict';

describe('Controller: VieweventsCtrl', function () {

  // load the controller's module
  beforeEach(module('myWellnessTrackerApp'));

  var VieweventsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    VieweventsCtrl = $controller('VieweventsCtrl', {
      $scope: scope
    });
  }));

  //it('should have the correct title', function () {
  //  expect(scope.title).toBe("View Appointments/Visits");
  //});
});
