'use strict';

describe('Controller: EventaddCtrl', function () {

  // load the controller's module
  beforeEach(module('myWellnessTrackerApp'));

  var EventaddCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EventaddCtrl = $controller('EventaddCtrl', {
      $scope: scope
    });
  }));

  //it('should have the correct title', function () {
  //  expect(scope.title).toBe("Add Appointment/Visit");
  //});
});
