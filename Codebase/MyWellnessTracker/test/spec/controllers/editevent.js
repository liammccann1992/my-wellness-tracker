'use strict';

describe('Controller: EditeventCtrl', function () {

  // load the controller's module
  beforeEach(module('myWellnessTrackerApp'));

  var EditeventCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EditeventCtrl = $controller('EditeventCtrl', {
      $scope: scope
    });
  }));

});
