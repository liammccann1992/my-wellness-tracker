'use strict';

describe('Controller: AddsideeffectCtrl', function () {

  // load the controller's module
  beforeEach(module('myWellnessTrackerApp'));

  var AddsideeffectCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AddsideeffectCtrl = $controller('AddsideeffectCtrl', {
      $scope: scope
    });
  }));

  //it('should have the correct title', function () {
  //  expect(scope.title).toBe("Add Side Effect");
  //});
});
