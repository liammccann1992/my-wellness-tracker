'use strict';

/**
 * @ngdoc overview
 * @name myWellnessTrackerApp
 * @description
 * # myWellnessTrackerApp
 *
 * Main module of the application.
 */

var app = angular
  .module('myWellnessTrackerApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'LocalStorageModule',
    'toastr',
    'angular.filter',
    '720kb.datepicker',
    'ngProgress',
    'cn.offCanvas',
    'tc.chartjs'
  ]);

app.config(function ($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'views/main.html',
      controller: 'MainCtrl',
      resolve: {
        check: function (checkRouting) {
          return checkRouting.check();
        }
      }
    })

    .when('/sideeffects', {
      templateUrl: 'views/sideeffects.html',
      controller: 'SideeffectsCtrl'
    })
    .when('/viewSideEffects', {
      templateUrl: 'views/viewsideeffects.html',
      controller: 'ViewsideeffectsCtrl'
    })
    .when('/AddSideEffect/:id', {
      templateUrl: 'views/addsideeffect.html',
      controller: 'AddsideeffectCtrl'
    })

    .when('/EventRecorder', {
      templateUrl: 'views/eventrecorder.html',
      controller: 'EventrecorderCtrl'
    })

    .when('/EventAdd/:id', {
      templateUrl: 'views/eventadd.html',
      controller: 'EventaddCtrl'
    })

    .when('/viewevents', {
      templateUrl: 'views/viewevents.html',
      controller: 'VieweventsCtrl'
    })
    .when('/ataglancesideeffects', {
      templateUrl: 'views/ataglancesideeffects.html',
      controller: 'atAGlanceSideEffectsCtrl'
    })
    .when('/sideEffectStatistics', {
      templateUrl: 'views/sideeffectstatistics.html',
      controller: 'SideeffectstatisticsCtrl'
    })
    .when('/Disclaimer', {
      templateUrl: 'views/disclaimer.html',
      controller: 'DisclaimerCtrl'
    })
    .when('/termsAndConditions', {
      templateUrl: 'views/termsandconditions.html',
      controller: 'TermsandconditionsCtrl'
    })
    .when('/settings', {
      templateUrl: 'views/settings.html',
      controller: 'SettingsCtrl'
    })
    .when('/about', {
      templateUrl: 'views/about.html',
      controller: 'AboutCtrl'
    })
    .when('/viewAllMyData', {
      templateUrl: 'views/viewallmydata.html',
      controller: 'ViewallmydataCtrl'
    })
    .when('/editEvent/:id', {
      templateUrl: 'views/editevent.html',
      controller: 'EditeventCtrl'
    })
    .when('/deleteEvent/:id', {
      templateUrl: 'views/deleteevent.html',
      controller: 'DeleteeventCtrl'
    })
    .otherwise({
      redirectTo: '/'
    });
});

app.config(function (localStorageServiceProvider) {
  localStorageServiceProvider
    .setPrefix('mwt');

  localStorageServiceProvider
    .setStorageType('localStorage');
});

app.config(function (toastrConfig) {
  angular.extend(toastrConfig, {
    extendedTimeOut: 1000,
    maxOpened: 3,
    newestOnTop: true,
    positionClass: 'toast-bottom-right',
    tapToDismiss: true,
    timeOut: 3000
  });
});


app.run(function ($rootScope, $location, $timeout) {
  $rootScope.$on('$viewContentLoaded', function () {
    $timeout(function () {
      $(document).foundation();

      $("select").click(function () {
        $(this).focus();
      });

      FastClick.attach(document.body);
    }, 500);
  });

  $rootScope.go = function (path) {
    $location.path(path);
  };
});


app.run(function ($rootScope, $location, $timeout) {
  $rootScope.$on('$viewContentLoaded', function () {
  });

  $rootScope.$on('destroy', function () {
  });

});

