'use strict';

/**
 * @ngdoc directive
 * @name myWellnessTrackerApp.directive:innerPageHeader
 * @description
 * # innerPageHeader
 */
angular.module('myWellnessTrackerApp')
  .directive('innerPageHeader' ,function () {
    return {
      templateUrl: 'templates/innerPageHeader.html',
      restrict: 'E'
    };
  });
