'use strict';

/**
 * @ngdoc service
 * @name myWellnessTrackerApp.DiaryService
 * @description
 * # DiaryService
 * Service in the myWellnessTrackerApp.
 */
angular.module('myWellnessTrackerApp')
  .service('DiaryService', function (dateHelperService, SideEffectService) {

    var service = {};

    service.buildDiary = function () {
      var blankCalendar = dateHelperService.buildCalendar(5);
      var sideEffects = SideEffectService.getAllFormatted();

      addSideEffectsToDiary(blankCalendar, sideEffects);

      return blankCalendar;
    }

    function addSideEffectsToDiary(diary, se) {
      for (var i = 0; i < diary.length; i++) {
        for (var j = 0; j < se.length; j++) {
          if (diary[i].Date.getDate() == se[j].jsDate.getDate()) {
            if (diary[i].sideEffects != null && diary[i].sideEffects != undefined) {
              diary[i].sideEffects.push(se[j]);
            } else {
              diary[i].sideEffects = new Array();
              diary[i].sideEffects.push(se[j]);
            }
          }
        }


      }
    }

    return service;

  });
