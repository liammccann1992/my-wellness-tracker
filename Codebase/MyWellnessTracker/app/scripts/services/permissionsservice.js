'use strict';

/**
 * @ngdoc service
 * @name myWellnessTrackerApp.permissionsService
 * @description
 * # permissionsService
 * Service in the myWellnessTrackerApp.
 */
angular.module('myWellnessTrackerApp')
  .service('permissionsService', function (localStorageService,dateHelperService) {
    var service = {};

    service.get = function(){
      var v = localStorageService.get("terms");
      if(v == null || v == undefined)
        return {};

      return v;
    };

    service.set = function(perms){
      localStorageService.set("terms",perms);
    };

    service.setTermsAndConditionsComplete = function(accepeted){
      var permis = service.get();

      if(permis.termsAndConditions == null || permis.termsAndConditions == undefined){
        permis.termsAndConditions = {};
      }

      if(accepeted){
        permis.termsAndConditions.agreed = true;
      }else{
        permis.termsAndConditions.agreed = false;
      }
      permis.termsAndConditions.when = dateHelperService.getCurrentDateTimeInUnix();

      service.set(permis);

    };

    service.termsAndConditionsComplete = function(){
      var permis = service.get();

      if(permis == undefined || permis == null || permis.termsAndConditions == null || permis.termsAndConditions == undefined || permis.termsAndConditions.agreed != true){
          return false;

      }
      return true;

    };

    service.setDisclaimerComplete = function(accepeted){
      var permis = service.get();

      if(permis == null || permis == undefined || permis.disclaimer == null || permis.disclaimer == undefined){
        permis.disclaimer = {};
      }


      if(accepeted){
        permis.disclaimer.agreed = true;
      }else{
        permis.disclaimer.agreed = false;
      }

      permis.disclaimer.when = dateHelperService.getCurrentDateTimeInUnix();

      service.set(permis);
    };

    service.disclaimerComplete = function(){
      var permis = service.get();

      if(permis.disclaimer == null || permis.disclaimer == undefined || permis.disclaimer.agreed != true){
        return false;

      }
      return true;
    };

    return service;
  });
