'use strict';

/**
 * @ngdoc service
 * @name myWellnessTrackerApp.dateHelperService
 * @description
 * # dateHelperService
 * Service in the myWellnessTrackerApp.
 */
angular.module('myWellnessTrackerApp')
  .service('dateHelperService', function () {
    var service = {};
    var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

    service.unixDateToUkDate = function(unixDate){
      var unix = unixDate * 1000;
      var date = new Date(unix);

      return service.dateToDate(date);
    };

    service.getCurrentDateTimeInUnix = function(){
      var currentDate = new Date();
      return currentDate.getTime()/1000;
    };

    service.getUnixDateCurrentDate = function(ukDate){
      var parts = ukDate.split("/");
      var dt = new Date(parseInt(parts[2], 10),
        parseInt(parts[1], 10) - 1,
        parseInt(parts[0], 10));

      return (dt.getTime()/1000);
    };

    service.dateToDateTime = function(d){
      return addZero(addZero(d.getDate()) + "/" + d.getMonth()+1)+"/"  + d.getFullYear() + " " +
        addZero(d.getHours()) + ":" + addZero(d.getMinutes()) + ":" + addZero(d.getMinutes());
    };

    service.dateToDate= function(d){
      return addZero(addZero(d.getDate() + "/" + (d.getMonth()+1)) + "/" + d.getFullYear());
    };

    service.dateToTime = function(d){
      return addZero(addZero(d.getHours()) + ":" + addZero(d.getMinutes()));
    }

    service.buildCalendar = function(pastFutureDayNum){
      var array = new Array();

      for(var i = pastFutureDayNum; i > 0 ; i--){
        var temp = new Date();
        temp.setDate(temp.getDate()+ i);
        array.push({
          "Date" : temp,
          "uiDate" : service.dateToDate(temp)
        });
      }

      array.push({
        "Date" : new Date(),
        "uiDate" : service.dateToDate(temp)
      });

      for(var i = 1 ; i <= pastFutureDayNum ; i++){
        var temp = new Date();
        temp.setDate(temp.getDate()-i);
        array.push({
          "Date" : temp,
          "FullTextDate" : service.dateToFullTextDate(temp)
        });
      }
      return array;
    }

    service.dateToFullTextDate = function(date){
      return days[date.getDay()]  + " " + date.getDate() + nth(date)  + " " + months[date.getMonth()];
    }

    function addZero(n){
      return n < 10 ? '0' + n : '' + n;
    }

    function nth(d) {
      if(d>3 && d<21) return 'th'; // thanks kennebec
      switch (d % 10) {
        case 1:  return "st";
        case 2:  return "nd";
        case 3:  return "rd";
        default: return "th";
      }
    }

     service.convert = function(d) {
      // Converts the date in d to a date-object. The input can be:
      //   a date object: returned without modification
      //  an array      : Interpreted as [year,month,day]. NOTE: month is 0-11.
      //   a number     : Interpreted as number of milliseconds
      //                  since 1 Jan 1970 (a timestamp)
      //   a string     : Any format supported by the javascript engine, like
      //                  "YYYY/MM/DD", "MM/DD/YYYY", "Jan 31 2009" etc.
      //  an object     : Interpreted as an object with year, month and date
      //                  attributes.  **NOTE** month is 0-11.
      return (
        d.constructor === Date ? d :
          d.constructor === Array ? new Date(d[0],d[1],d[2]) :
            d.constructor === Number ? new Date(d) :
              d.constructor === String ? new Date(d) :
                typeof d === "object" ? new Date(d.year,d.month,d.date) :
                  NaN
      );
    }
    service.compare = function(a,b) {
      // Compare two dates (could be of any type supported by the convert
      // function above) and returns:
      //  -1 : if a < b
      //   0 : if a = b
      //   1 : if a > b
      // NaN : if a or b is an illegal date
      // NOTE: The code inside isFinite does an assignment (=).
      return (
        isFinite(a=service.convert(a).valueOf()) &&
        isFinite(b=service.convert(b).valueOf()) ?
        (a>b)-(a<b) :
          NaN
      );
    };
//http://stackoverflow.com/questions/492994/compare-dates-with-javascript
    service.inRange = function(d,start,end) {
      // Checks if date in d is between dates in start and end.
      // Returns a boolean or NaN:
      //    true  : if d is between start and end (inclusive)
      //    false : if d is before start or after end
      //    NaN   : if one or more of the dates is illegal.
      // NOTE: The code inside isFinite does an assignment (=).
      return (
        isFinite(d=this.convert(d).valueOf()) &&
        isFinite(start=this.convert(start).valueOf()) &&
        isFinite(end=this.convert(end).valueOf()) ?
        start <= d && d <= end :
          NaN
      );
    }

    service.isToday = function(date){
      var currentDate = new Date();
      if(service.dateToDate(date) == service.dateToDate(currentDate)){
        return true;
      }

      return false;
    }

    return service;

  });
