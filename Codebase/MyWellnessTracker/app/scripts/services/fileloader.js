'use strict';

/**
 * @ngdoc service
 * @name myWellnessTrackerApp.fileLoader
 * @description
 * # fileLoader
 * Service in the myWellnessTrackerApp.
 */
angular.module('myWellnessTrackerApp')
  .service('fileLoader', function($http,$q,$rootScope) {

    var fileLoader = this;

    fileLoader.getFile = function(name){
      var defer = $q.defer();

      $http.get("data/" + name + ".json")
        .success(function(res){
          defer.resolve(res);
        })
        .error(function(err,status){
          defer.reject(err);
        });


      return defer.promise;
    }
    ;

    return fileLoader;

  });


