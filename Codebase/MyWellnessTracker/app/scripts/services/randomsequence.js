'use strict';

/**
 * @ngdoc service
 * @name myWellnessTrackerApp.randomSequence
 * @description
 * # randomSequence
 * Service in the myWellnessTrackerApp.
 */
angular.module('myWellnessTrackerApp')
  .service('randomGenerator', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var service = {};

    ///http://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
    service.randomLongString = function(){
      return service.randomShortString () + service.randomShortString () + '-' + service.randomShortString () + '-' + service.randomShortString () + '-' +
        service.randomShortString () + '-' + service.randomShortString () + service.randomShortString () + service.randomShortString ();
    }

    service.randomShortString = function(){
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }

    return service;
  });
