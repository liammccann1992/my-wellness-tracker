'use strict';

/**
 * @ngdoc function
 * @name myWellnessTrackerApp.controller:DisclaimerCtrl
 * @description
 * # DisclaimerCtrl
 * Controller of the myWellnessTrackerApp
 */
angular.module('myWellnessTrackerApp')
  .controller('DisclaimerCtrl', function ($rootScope,$scope,permissionsService,toastr) {
    $scope.title = "Disclaimer";


    $scope.accept = function(){
      permissionsService.setDisclaimerComplete(true);
      toastr.info("","You have agreed to My Wellness Tracker's Disclaimer");
      $rootScope.go("");
    }
  });
