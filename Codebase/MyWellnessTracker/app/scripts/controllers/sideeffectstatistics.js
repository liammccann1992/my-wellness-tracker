'use strict';

/**
 * @ngdoc function
 * @name myWellnessTrackerApp.controller:SideeffectstatisticsCtrl
 * @description
 * # SideeffectstatisticsCtrl
 * Controller of the myWellnessTrackerApp
 */
angular.module('myWellnessTrackerApp')
  .controller('SideeffectstatisticsCtrl', function ($scope,SideEffectService,offCanvas) {
    $scope.title = "Side Effect Statistics";
    $scope.informationPopup = {};

    $scope.showNavToggler = true;
    $scope.offCanvas = offCanvas.create("viewsideeffects");


    $scope.recordedSideEffects = [];
    $scope.firstRecordedSideEffect = null;
    $scope.lastRecordedSideEffect = null;

    $scope.init = function(){
      var all = SideEffectService.getAll();
      var grouped = SideEffectService.totalSideEffects(all);

      $scope.recordedSideEffects = grouped;
      $scope.firstRecordedSideEffect = SideEffectService.getFirstRecordedSideEffect();
      $scope.lastRecordedSideEffect = SideEffectService.getLastRecordedSideEffect();

    }

    $scope.init();
    $scope.toggle = function(){
      $scope.offCanvas.toggle();
    }
  });
