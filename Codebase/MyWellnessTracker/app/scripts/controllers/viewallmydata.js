'use strict';

/**
 * @ngdoc function
 * @name myWellnessTrackerApp.controller:ViewallmydataCtrl
 * @description
 * # ViewallmydataCtrl
 * Controller of the myWellnessTrackerApp
 */
angular.module('myWellnessTrackerApp')
  .controller('ViewallmydataCtrl', function ($scope,localStorageService) {

    $scope.title = "My Data";

    $scope.allData = [];

    $scope.init = function(){

      var lsKeys = localStorageService.keys();
      console.log(lsKeys);

      for(var i = 0 ; i < lsKeys.length;i++){

        var key =  lsKeys[i];
        var obj = localStorageService.get(key);
        var objString = JSON.stringify(obj,null,4);
        $scope.allData.push({
          "Title" : key,
          "Data" : syntaxHighlight(objString)
        })
        console.log(localStorageService.get(lsKeys[i]));
      }


    };

    //http://stackoverflow.com/questions/4810841/how-can-i-pretty-print-json-using-javascript
    function syntaxHighlight(json){
      json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
      return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
          if (/:$/.test(match)) {
            cls = 'key';
          } else {
            cls = 'string';
          }
        } else if (/true|false/.test(match)) {
          cls = 'boolean';
        } else if (/null/.test(match)) {
          cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
      });
    }


    $scope.init();
  });
