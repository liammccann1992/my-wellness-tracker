'use strict';

/**
 * @ngdoc function
 * @name myWellnessTrackerApp.controller:ViewsideeffectsCtrl
 * @description
 * # ViewsideeffectsCtrl
 * Controller of the myWellnessTrackerApp
 */
angular.module('myWellnessTrackerApp')
  .controller('ViewsideeffectsCtrl', function ($scope,SideEffectService,ngProgress,pageService,offCanvas) {
    $scope.title = 'View Recorded Side Effects';


    $scope.recordedSideEffects = [];
    $scope.showNavToggler = true;
    $scope.offCanvas = offCanvas.create("viewsideeffects");

    $scope.init = function(){
      var all = SideEffectService.getAllFormatted();
      $scope.recordedSideEffects = all;

      pageService.getPageInformation("viewsideeffects").then(function(res){
        $scope.informationPopup = res;
      },function(err){
        console.log(err);
      });

      //ngProgress.complete();
    }

    $scope.init();

    $scope.go = function(path){
   //   ngProgress.start();
      $rootScope.go(path);
    }


    $scope.toggle = function(){
      $scope.offCanvas.toggle();
    }
  });
