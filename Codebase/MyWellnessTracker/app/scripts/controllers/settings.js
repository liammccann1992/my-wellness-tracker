'use strict';

/**
 * @ngdoc function
 * @name myWellnessTrackerApp.controller:SettingsCtrl
 * @description
 * # SettingsCtrl
 * Controller of the myWellnessTrackerApp
 */
angular.module('myWellnessTrackerApp')
  .controller('SettingsCtrl', function ($rootScope,$scope,localStorageService,toastr) {
    $scope.title = "Settings";

    $scope.go = function(path){
      $rootScope.go(path);
    }

    $scope.clearData = function(){
      localStorageService.clearAll();
      toastr.info("", "All data cleared");
      $rootScope.go("");

    }
  });
