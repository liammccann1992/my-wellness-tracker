'use strict';

/**
 * @ngdoc function
 * @name myWellnessTrackerApp.controller:DeleteeventCtrl
 * @description
 * # DeleteeventCtrl
 * Controller of the myWellnessTrackerApp
 */
angular.module('myWellnessTrackerApp')
  .controller('DeleteeventCtrl', function ($rootScope,$scope,$routeParams,eventService ,toastr ) {
    $scope.eventId = $routeParams.id;
    $scope.eventName = null;
    $scope.title = "Delete Appointment/Visit";

    $scope.event = {};

    $scope.init = function() {
      console.log("INIT: " + $scope.eventId);
      $scope.event = eventService.getById($scope.eventId);
    }

    $scope.init();


    $scope.ok = function(){
      console.log("ID: " + $scope.eventId);
      eventService.delete($scope.eventId);
      toastr.info('Success!', $scope.event.name  + ' has been delete');
      $rootScope.go("viewevents");
    }

    $scope.cancel = function(){
      toastr.info("", "Delete Appointment/Visit cancelled");
      $rootScope.go("viewevents");
    }
  });
