'use strict';

/**
 * @ngdoc function
 * @name myWellnessTrackerApp.controller:EventrecorderCtrl
 * @description
 * # EventrecorderCtrl
 * Controller of the myWellnessTrackerApp
 */

angular.module('myWellnessTrackerApp')
  .controller('EventrecorderCtrl', function ($rootScope,$scope, fileLoader,ngProgress,pageService) {

    $scope.init = function () {
      fileLoader.getFile("events")
        .then(function (res) {
          $scope.events = res;

        }, function (err) {
          //Error
        });

      pageService.getPageInformation("eventrecorder").then(function(res){
        $scope.informationPopup = res;
      },function(err){
        console.log(err);
      });

    //  ngProgress.complete();
    }

    $scope.init();

    $scope.title = 'Record Appointment/Visits';
    $scope.showSideNav = true;

    $scope.selectedItem = null;
    $scope.events = [];


    $scope.go = function(path){
    //  ngProgress.start();
      $rootScope.go(path);
    }
    //$scope.$on("$destroy", function() {
    //  ngProgress.start();
    //});
  });
