'use strict';

/**
 * @ngdoc function
 * @name myWellnessTrackerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the myWellnessTrackerApp
 */
angular.module('myWellnessTrackerApp')
  .controller('MainCtrl', function ($rootScope,$scope,ngProgress) {
    $scope.title = 'Home';
    $scope.showSideNav = false;
    $scope.hideHomeIcon = true;

    $scope.init = function(){
     // ngProgress.complete();
    };

    $scope.init();

    $scope.toggleSideNav = function(){
      console.log("Hello");
    }

    $scope.go = function(path){
     // ngProgress.start();
      $rootScope.go(path);
    }
    //$scope.$on("$destroy", function() {
    //  ngProgress.start();
    //});
  });
