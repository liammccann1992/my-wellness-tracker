/*
ngProgress 1.0.7 - slim, site-wide progressbar for AngularJS
(C) 2013 - Victor Bjelkholm
License: MIT
Source: https://github.com/VictorBjelkholm/ngProgress
Date Compiled: 2014-09-26
*/
angular.module('ngProgress.provider', ['ngProgress.directive'])
    .provider('ngProgress', function () {
        'use strict';
        //Default values for provider
        this.autoStyle = false;
        this.height = '100px';
        this.color = '#FFFFFF';

        this.$get = ['$document',
            '$window',
            '$compile',
            '$rootScope',
            '$timeout', function ($document, $window, $compile, $rootScope, $timeout) {
                var height = this.height,
                color = this.color,
                $scope = $rootScope,
                parent = $document.find('body')[0];

            // Compile the directive
            var progressbarEl = $compile('<ng-progress></ng-progress>')($scope);
            // Add the element to body
            parent.appendChild(progressbarEl[0]);
            // Set the initial height

            //// If height or color isn't undefined, set the height, background-color and color.
            //if (height !== undefined) {
            //    progressbarEl.eq(0).children().css('height', height);
            //}
            //if (color !== undefined) {
            //    progressbarEl.eq(0).children().css('background-color', color);
            //    progressbarEl.eq(0).children().css('color', color);
            //}
            // The ID for the interval controlling start()
            var intervalCounterId = 0;
            var animation;
            return {
                start: function () {
                    this.show();
                },
                // Sets the height of the progressbar. Use any valid CSS value
                // Eg '10px', '1em' or '1%'
                height: function (new_height) {
                    if (new_height !== undefined) {
                        height = new_height;
                        $scope.height = height;
                        if(!$scope.$$phase) {
                            $scope.$apply();
                        }
                    }
                    return height;
                },
                // Sets the color of the progressbar and it's shadow. Use any valid HTML
                // color
                color: function (new_color) {
                    if (new_color !== undefined) {
                        color = new_color;
                        $scope.color = color;
                        if(!$scope.$$phase) {
                            $scope.$apply();
                        }
                    }
                    return color;
                },
                hide: function () {
                   //progressbarEl.children().css('opacity', '0');
                    var self = this;
                    self.animate(function () {
                      console.log(progressbarEl.html());
                      $timeout(function(){
                        progressbarEl.hide();
                      },500);

                    }, 250);
                },
                show: function () {
                    var self = this;
                  progressbarEl.show();
                    //self.animate(function () {
                    //  progressbarEl.show();
                    //   // progressbarEl.children().css('opacity', '1');
                    //}, 100);
                },
                // Cancel any prior animations before running new ones.
                // Multiple simultaneous animations just look weird.
                animate: function(fn, time) {
                    if(animation) { $timeout.cancel(animation); }
                    animation = $timeout(fn, time);
                },
                css: function (args) {
                    return progressbarEl.children().css(args);
                },
                complete: function () {


                    var self = this;
                    clearInterval(intervalCounterId);
                    $timeout(function () {
                        self.hide();
                        $timeout(function () {
                            //count = 0;
                            //self.updateCount(count);
                        }, 500);
                    }, 1000);
                    return 0;
                },
                //set the parent of the directive, sometimes body is not sufficient
                setParent: function(newParent) {
                    if(newParent === null || newParent === undefined) {
                        throw new Error('Provide a valid parent of type HTMLElement');
                    }

                    if(parent !== null && parent !== undefined) {
                        parent.removeChild(progressbarEl[0]);
                    }

                    parent = newParent;
                    parent.appendChild(progressbarEl[0]);
                },
                getDomElement: function () {
                    return progressbarEl;
                }
            };
        }];

        this.setColor = function (color) {
            if (color !== undefined) {
                this.color = color;
            }

            return this.color;
        };

        this.setHeight = function (height) {
            if (height !== undefined) {
                this.height = height;
            }

            return this.height;
        };
    });
angular.module('ngProgress.directive', [])
    .directive('ngProgress', ["$window", "$rootScope", function ($window, $rootScope) {
        var directiveObj = {
            // Replace the directive
            replace: true,
            // Only use as a element
            restrict: 'E',
            link: function ($scope, $element, $attrs, $controller) {
                // Watch the count on the $rootScope. As soon as count changes to something that
                // isn't undefined or null, change the counter on $scope and also the width of
                // the progressbar. The same goes for color and height on the $rootScope
                $rootScope.$watch('count', function (newVal) {
                    if (newVal !== undefined || newVal !== null) {
                        $scope.counter = newVal;
                        $element.eq(0).children().css('width', newVal + '%');
                    }
                });
                $rootScope.$watch('color', function (newVal) {
                    if (newVal !== undefined || newVal !== null) {
                        $scope.color = newVal;
                        $element.eq(0).children().css('background-color', newVal);
                        $element.eq(0).children().css('color', newVal);
                    }
                });
                $rootScope.$watch('height', function (newVal) {
                    if (newVal !== undefined || newVal !== null) {
                        $scope.height = newVal;
                        $element.eq(0).children().css('height', newVal);
                    }
                });
            },
            // The actual html that will be used
            template: '<div id="ngProgress-container">' +
                          '<div id="ngProgress">' +
                                '<div class="ngProgressInner">' +
                                  '<img src="images/spinner.gif"/> ' +

                                '</div>' +
                          '</div>' +
                      '</div>'
        };
        return directiveObj;
    }]);

angular.module('ngProgress', ['ngProgress.directive', 'ngProgress.provider']);
