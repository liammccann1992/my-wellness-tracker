describe("LiamsMath", function() {
  var liamsMath;

  beforeEach(function() {
      liamsMath = new LiamsMath();
  });

    it("should be able to add Positive numbers", function(){
        var res = liamsMath.add(1,1);
        expect(res).toEqual(2);
    });

    it("should not be able to divide by zero", function(){
        expect(function() {
            liamsMath.divide(10,0);
        }).toThrowError("cannot divide by zero");
    });

    it("should be able to divide by a number greater than zero", function(){
        expect(liamsMath.divide(10,2)).toEqual(5);
    });

});
