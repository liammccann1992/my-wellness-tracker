function LiamsMath() {
}

LiamsMath.prototype.add = function(d1,d2) {
    return d1 + d2;
};

LiamsMath.prototype.divide = function(main,divideBy){
    if(divideBy == 0)
        throw new Error("cannot divide by zero");

    return main/divideBy;
}