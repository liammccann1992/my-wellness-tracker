/**
 * Created by Liam McCann on 22/12/2014.
 */
var app = angular.module('FoursquareApp',["ngRoute","ngResource","toaster"]);
app.config(function ($routeProvider) {

    $routeProvider.when("/Home", {
        controller: "homeController",
        templateUrl: "app/views/home.html"
    });

    $routeProvider.when("/Toxicity", {
        controller: "toxicityController",
        templateUrl: "app/views/toxicity.html"
    });

    $routeProvider.when("/Appointments", {
        controller: "appointmentsController",
        templateUrl: "app/views/appointments.html"
    });
    $routeProvider.otherwise({ redirectTo: "/Home" });

});

app.run(function($rootScope) {
    $rootScope.$on('$viewContentLoaded', function () {
        $(document).foundation();
    });
});