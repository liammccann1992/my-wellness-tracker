/**
 * Created by Liam McCann on 22/12/2014.
 */
app.controller('toxicityController', function ($scope,toxicityService,toaster) {
    $scope.sortBy = "Name";

    $scope.addSickness = function(hours){
        toaster.pop('success', "Entry Added", "Sickness approx " + hours + " hours ago has been logged");
       // toaster.pop('error', "title", "text");
       // toaster.pop('warning', "title", "text");
       // toaster.pop('note', "title", "text");
    };

    $scope.addDiarrhea = function(hours){
        toaster.pop('success', "Entry Added", "Diarrhea approx " + hours + " hours ago has been logged");
        // toaster.pop('error', "title", "text");
        // toaster.pop('warning', "title", "text");
        // toaster.pop('note', "title", "text");
    };
});