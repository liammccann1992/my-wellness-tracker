'use strict';

/**
 * @ngdoc function
 * @name myWellnessTrackerApp.controller:AddsideeffectCtrl
 * @description
 * # AddsideeffectCtrl
 * Controller of the myWellnessTrackerApp
 */
angular.module('myWellnessTrackerApp')
  .controller('AddsideeffectCtrl', function ($rootScope,$scope,$routeParams,SideEffectService,fileLoader,toastr, ngProgress,pageService) {
    $scope.title = 'Add Side Effect';
    $scope.showSideNav = true;
    $scope.SideEffects = [];

    $scope.sideEffectId = $routeParams.id;
    $scope.sideEffectName = "";


    $scope.datePattern = "dd/MM/yyyy";
    $scope.dateMax = new Date().yyyymmdd();
    $scope.defalutDate = new Date().yyyymmdd();

    $scope.init = function () {
      fileLoader.getFile("sideEffects")
        .then(function (res) {
          $scope.SideEffects = res;
          assignNameToChooseSideEffect();
        }, function (err) {
          //Error
        });

      pageService.getPageInformation("addsideeffect").then(function(res){
        $scope.informationPopup = res;
      },function(err){
        console.log(err);
      });

     // ngProgress.complete();
    }

    $scope.init();

    $scope.ok = function(){
      var date = $scope.toAdd.date;
      var times = $scope.toAdd.times;
      SideEffectService.add($scope.sideEffectId,date,times)
      toastr.info("", $scope.sideEffectName + " has been logged");
      $scope.go("sideeffects");
    }

    $scope.cancel = function(){
      $scope.go("sideeffects");
      toastr.info("", "Add Side Effect cancelled");
    }

    function assignNameToChooseSideEffect() {
      for (var i = 0; i < $scope.SideEffects.length; i++) {
        if ($scope.sideEffectId != null && $scope.sideEffectId != undefined) {
          if ($scope.SideEffects[i].id == $scope.sideEffectId) {
            $scope.sideEffectName = $scope.SideEffects[i].name;
          }
        }
      }
    }

    $scope.go = function(path){
    //  ngProgress.start();
      $rootScope.go(path);
    }
    //$scope.$on("$destroy", function() {
    //  ngProgress.start();
    //});

  });
