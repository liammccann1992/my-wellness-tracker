'use strict';

/**
 * @ngdoc function
 * @name myWellnessTrackerApp.controller:EventaddCtrl
 * @description
 * # EventaddCtrl
 * Controller of the myWellnessTrackerApp
 */
angular.module('myWellnessTrackerApp')
  .controller('EventaddCtrl', function ($rootScope, $scope,$routeParams,fileLoader,eventService,toastr,ngProgress,pageService) {
    $scope.eventId = $routeParams.id;
    $scope.eventName = null;
    $scope.title = "Add Appointment/Visit"

    $scope.init = function () {
      fileLoader.getFile("events")
        .then(function (res) {
          $scope.events = res;
          assignNameToChooseEvent();
        }, function (err) {
          //Error
        });

      pageService.getPageInformation("eventadd").then(function(res){
        $scope.informationPopup = res;
      },function(err){
        console.log(err);
      });
    }

    $scope.init();


    $scope.go = function(path){
     // ngProgress.start();
      $rootScope.go(path);
    }


    $scope.ok = function(){
      eventService.add($scope.eventId,$scope.event);
      toastr.success('Success!', $scope.eventName  + ' has been recorded');
      $scope.go("EventRecorder");
    }

    $scope.cancel = function(){
      $scope.go("EventRecorder");
      toastr.info("", "Add Appointment/Visit cancelled");
    }


    function assignNameToChooseEvent() {
      for (var i = 0; i < $scope.events.length; i++) {
        if ($scope.eventId != null && $scope.eventId != undefined) {
          if ($scope.events[i].id == $scope.eventId) {
            $scope.eventName = $scope.events[i].name;
          }
        }
      }
    }
  });
