'use strict';

/**
 * @ngdoc function
 * @name myWellnessTrackerApp.controller:VieweventsCtrl
 * @description
 * # VieweventsCtrl
 * Controller of the myWellnessTrackerApp
 */
angular.module('myWellnessTrackerApp')
  .controller('VieweventsCtrl', function ($rootScope,$scope,eventService,ngProgress,pageService) {
    $scope.title = 'View Appointments/Visits';

    $scope.recordedEvents = [];
    $scope.recordEventsOnView = [];

    $scope.showNavToggler = false;
    $scope.showNav = true;


    $scope.toggle = function(){
      $scope.showNav = !$scope.showNav;
    }

    $scope.recordedEvents_last60 = [];
    $scope.recordedEvents_last30 = [];
    $scope.recordedEvents_last15 = [];
    $scope.recordedEvents_today = [];
    $scope.recordedEvents_next15 = [];
    $scope.recordedEvents_next30 = [];
    $scope.recordedEvents_next60 = [];
    $scope.recordedEvents_all = [];

    $scope.eventsViewData = [];

    $scope.eventPeriods = [];
    $scope.selectedEventPeriod = null;

    $scope.buildEventsPeriods = function(){

      if($scope.recordedEvents_last60.length > 0){
        $scope.eventPeriods.push({
          "label" : "Last 60 Days",
          "days" : -60,
          "past" : true
        });
      }

      if($scope.recordedEvents_last30.length > 0){
        $scope.eventPeriods.push({
          "label" : "Last 30 Days",
          "days" : -30,
          "past" : true
        });
      }

      if($scope.recordedEvents_last15.length > 0){
        $scope.eventPeriods.push({
          "label" : "Last 15 Days",
          "days" : -15,
          "past" : true
        });
      }
      if($scope.recordedEvents_today.length > 0){
        $scope.eventPeriods.push({
          "label" : "Today",
          "days" : 0,
          "past" : false
        });

        //Set today as defalut
        $scope.selectedEventPeriod = $scope.eventPeriods[$scope.eventPeriods.length-1];
      }

      if($scope.recordedEvents_next15.length > 0){
        $scope.eventPeriods.push({
          "label" : "Next 15 Days",
          "days" : 15,
          "past" : false
        });
      }

      if($scope.recordedEvents_next30.length > 0){
        $scope.eventPeriods.push({
          "label" : "Next 30 Days",
          "days" : 30,
          "past" : false
        });
      }

      if($scope.recordedEvents_next60.length > 0){
        $scope.eventPeriods.push({
          "label" : "Next 60 Days",
          "days" : 60,
          "past" : false
        });
      }

      //Handle no values
      if($scope.eventPeriods.length == 0){
        $scope.eventPeriods.push({
          "label" : "Not enough data, Please record more appointments/visits"
        })
      }else{
        $scope.eventPeriods.push({
          "label" : "All",
          "days" : 'All',
          "past" : false
        });
      }

      if($scope.selectedEventPeriod == null ||$scope.selectedEventPeriod.length == 0){
        $scope.selectedEventPeriod = $scope.eventPeriods[$scope.eventPeriods.length-1];
      }

      $scope.changeEventPeriod();
    }

    $scope.changeEventPeriod = function(){
      var value = $scope.selectedEventPeriod;
      var days = value.days;

      switch(days){
        case -60:
          $scope.eventsViewData = $scope.recordedEvents_last60;
          break;
        case -30:
          $scope.eventsViewData = $scope.recordedEvents_last30;
          break;
        case -15:
          $scope.eventsViewData= $scope.recordedEvents_last15;
          break;
        case 0:
          $scope.eventsViewData = $scope.recordedEvents_today;
          break;
        case 15:
          $scope.eventsViewData = $scope.recordedEvents_next15;
          break;
        case 30:
          $scope.eventsViewData = $scope.recordedEvents_next30;
          break;
        case 60:
          $scope.eventsViewData = $scope.recordedEvents_next60;
          break;
        case 'All':
              $scope.eventsViewData = $scope.recordedEvents_all;
      }

    }



    $scope.init = function(){
      var all = eventService.getAllFormatted();
      $scope.recordedEvents_all = all;
      $scope.recordedEvents_Last60 =  eventService.filterNDays(all,60,true);
      $scope.recordedEvents_last30 = eventService.filterNDays(all,30,true);
      $scope.recordedEvents_last15 = eventService.filterNDays(all,15,true);
      $scope.recordedEvents_today = eventService.filterNDays(all,0);
      $scope.recordedEvents_next15 = eventService.filterNDays(all,15,false);
      $scope.recordedEvents_next30 = eventService.filterNDays(all,30,false);
      $scope.recordedEvents_next60 = eventService.filterNDays(all,60,false);

      $scope.buildEventsPeriods();

      pageService.getPageInformation("viewevents").then(function(res){
        $scope.informationPopup = res;
      },function(err){
        console.log(err);
      });
      //ngProgress.complete();
    }

    $scope.init();

    $scope.go = function(path){
      //ngProgress.start();
      $rootScope.go(path);
    }
  });
