'use strict';

/**
 * @ngdoc function
 * @name myWellnessTrackerApp.controller:SideeffectsCtrl
 * @description
 * # SideeffectsCtrl
 * Controller of the myWellnessTrackerApp
 */
angular.module('myWellnessTrackerApp')
  .controller('SideeffectsCtrl', function ($rootScope,$scope,fileLoader,ngProgress,pageService) {

    $scope.title = 'Side Effects';
    $scope.showSideNav = true;
    $scope.SideEffects = [];
    $scope.informationPopup = {};

    $scope.init = function(){
      fileLoader.getFile("sideEffects")
        .then(function(res){
          $scope.SideEffects = res;
        }, function(err){
          console.log(err);
        });

     // ngProgress.complete();

      pageService.getPageInformation("sideeffects").then(function(res){
        $scope.informationPopup = res;
      },function(err){
        console.log(err);
      });
    };

    $scope.init();



    $scope.getSideEffects = function(){
      fileLoader.getFile("sideEffects")
        .then(function(res){
          //Success
          console.log(res);
          $scope.SideEffects = res;
        }, function(err){
          //Error
        });
    }

    $scope.go = function(path){
      //ngProgress.start();
      $rootScope.go(path);
    }
    //$scope.$on("$destroy", function() {
    //  ngProgress.start();
    //});
  });
