'use strict';

/**
 * @ngdoc function
 * @name myWellnessTrackerApp.controller:StatisticssideeffectsCtrl
 * @description
 * # StatisticssideeffectsCtrl
 * Controller of the myWellnessTrackerApp
 */
angular.module('myWellnessTrackerApp')
  .controller('atAGlanceSideEffectsCtrl', function ($scope, graphService,pageService,offCanvas) {

    $scope.title = "Side Effect At A Glance";
    $scope.informationPopup = {};

    $scope.showNavToggler = true;
    $scope.offCanvas = offCanvas.create("viewsideeffects");

    $scope.toggle = function(){
      $scope.offCanvas.toggle();
    }

    $scope.sideEffectsPieChartLastNDaysDataToggler = true;
    $scope.sideEffectsGraphLastNDaysDataToggler = true;


    $scope.sideEffectsPieChartLastNDaysToggle = function(){
      $scope.sideEffectsPieChartLastNDaysDataToggler = !$scope.sideEffectsPieChartLastNDaysDataToggler;
    }

    $scope.sideEffectsGraphLastNDaysDataToggle = function(){;
      $scope.sideEffectsGraphLastNDaysDataToggler = !$scope.sideEffectsGraphLastNDaysDataToggler;
    }

    $scope.sideEffectsPieChartLas3DaysData = [];
    $scope.sideEffectsPieChartLast5DaysData = [];
    $scope.sideEffectsPieChartLast15DaysData = [];
    $scope.sideEffectsPieChartLast30DaysData = [];
    $scope.sideEffectsPieChartLast60DaysData = [];
    $scope.sideEffectsPieChartAllData = [];
    $scope.pieChartPeriods = [];
    $scope.selectedPieChartPeriod = {};


    $scope.sideEffectsTotalData = [];
    $scope.sideEffectsOverTime = {
      labels: [],
      datasets: [
        {
          label: 'My First dataset',
          fillColor: 'rgba(170,206,161,1)',
          strokeColor: 'rgba(220,220,220,1)',
          pointColor: 'rgba(220,220,220,1)',
          pointStrokeColor: '#fff',
          pointHighlightFill: '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data: []
        }
      ]
    };

    $scope.buildPieChartPeriods = function(){
      if($scope.sideEffectsPieChartLast3DaysData.length > 0){
        $scope.pieChartPeriods.push({
          "label" : "Last 3 Days",
          "days" : 3,
          "past" : true
        });
      }

      if($scope.sideEffectsPieChartLast5DaysData.length > 0){
        $scope.pieChartPeriods.push({
          "label" : "Last 5 Days",
          "days" : 5,
          "past" : true
        });
      }

      if($scope.sideEffectsPieChartLast15DaysData.length > 0){
        $scope.pieChartPeriods.push({
          "label" : "Last 15 Days",
          "days" : 15,
          "past" : true
        });
      }

      if($scope.sideEffectsPieChartLast30DaysData.length > 0){
        $scope.pieChartPeriods.push({
          "label" : "Last 30 Days",
          "days" : 30,
          "past" : true
        });
      }

      if($scope.sideEffectsPieChartLast60DaysData.length > 0){
        $scope.pieChartPeriods.push({
          "label" : "Last 60 Days",
          "days" : 60,
          "past" : true
        });
      }


      //Handle no values
      if($scope.pieChartPeriods.length == 0){
        $scope.pieChartPeriods.push({
          "label" : "Not enough data, Please record more side effects"
        })
      }else{
        $scope.pieChartPeriods.push({
          "label" : "All",
          "days" : "All"
        });
      }

      $scope.selectedPieChartPeriod = $scope.pieChartPeriods[0];
    }

    $scope.changePieChartPeriod = function(){
      var value = $scope.selectedPieChartPeriod;
      var days = value.days;

      switch(days){
        case 3:
          $scope.sideEffectsTotalData = $scope.sideEffectsPieChartLast3DaysData;
          break;
        case 5:
          $scope.sideEffectsTotalData = $scope.sideEffectsPieChartLast5DaysData;
              break;
        case 15:
          $scope.sideEffectsTotalData = $scope.sideEffectsPieChartLast15DaysData;
              break;
        case 30:
          $scope.sideEffectsTotalData = $scope.sideEffectsPieChartLast30DaysData;
              break;
        case 60:
          $scope.sideEffectsTotalData = $scope.sideEffectsPieChartLast60DaysData;
              break;
        case "All":
          $scope.sideEffectsTotalData = $scope.sideEffectsPieChartAllData;
              break;
      }

    }



    $scope.myOptions = {
      responsive: true,
      animation: true

  };

    $scope.init = function () {
      $scope.sideEffectsPieChartLast3DaysData = graphService.sideEffectsLastNDaysToPieChart(3);
      $scope.sideEffectsPieChartLast5DaysData = graphService.sideEffectsLastNDaysToPieChart(5);
      $scope.sideEffectsPieChartLast15DaysData = graphService.sideEffectsLastNDaysToPieChart(15);
      $scope.sideEffectsPieChartLast30DaysData = graphService.sideEffectsLastNDaysToPieChart(30);
      $scope.sideEffectsPieChartLast60DaysData = graphService.sideEffectsLastNDaysToPieChart(60);
      $scope.sideEffectsPieChartAllData = graphService.sideEffectsLastNDaysToPieChart(null);

      $scope.buildPieChartPeriods();

      var temp = graphService.sideEffectsLastNDaysToGraph(7);
      $scope.sideEffectsOverTime.labels = temp.labels;
      $scope.sideEffectsOverTime.datasets[0].data = temp.data;

      $scope.changePieChartPeriod();

      pageService.getPageInformation("atAGlance").then(function(res){
        $scope.informationPopup = res;
      },function(err){
        console.log(err);
      });
    }

    $scope.init();

  });
