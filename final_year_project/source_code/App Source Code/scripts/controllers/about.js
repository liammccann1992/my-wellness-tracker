'use strict';

/**
 * @ngdoc function
 * @name myWellnessTrackerApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the myWellnessTrackerApp
 */
angular.module('myWellnessTrackerApp')
  .controller('AboutCtrl', function ($scope) {

    $scope.title = "About";

    $scope.hideInfoPopup = true;
  });
