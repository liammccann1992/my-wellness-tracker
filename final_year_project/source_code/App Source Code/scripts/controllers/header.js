'use strict';

/**
 * @ngdoc function
 * @name myWellnessTrackerApp.controller:HeaderctrlCtrl
 * @description
 * # HeaderctrlCtrl
 * Controller of the myWellnessTrackerApp
 */
angular.module('myWellnessTrackerApp')
  .controller('HeaderCtrl', function ($scope,$location) {

    $scope.showHome = false;

    $scope.$on('$routeChangeStart', function(next, current) {
      $scope.showHome = current.controller != "MainCtrl"
      &&
      current.controller   != "DisclaimerCtrl" &&
      current.controller != "TermsandconditionsCtrl";
    });

    $scope.back = function() {
      window.history.back();
    };
  });
