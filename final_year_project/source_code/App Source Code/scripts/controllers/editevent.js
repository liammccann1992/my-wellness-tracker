'use strict';

/**
 * @ngdoc function
 * @name myWellnessTrackerApp.controller:EditeventCtrl
 * @description
 * # EditeventCtrl
 * Controller of the myWellnessTrackerApp
 */
angular.module('myWellnessTrackerApp')
  .controller('EditeventCtrl', function ($rootScope,$scope,$routeParams,eventService,toastr) {

    $scope.eventId = $routeParams.id;
    $scope.event = {};
    $scope.title = "Edit Appointment/Visit"

    $scope.ok = function(){
      eventService.edit($scope.eventId,$scope.event);
      toastr.info('Success!', $scope.event.name  + ' has been edited');
      $scope.go("viewevents");
    }

    $scope.cancel = function(){
      toastr.info("", "Edit Appointment/Visit cancelled");
      $rootScope.go("viewevents");
    }



    $scope.init = function() {
      $scope.event = eventService.getById($scope.eventId);
    }

    $scope.init();
  });
