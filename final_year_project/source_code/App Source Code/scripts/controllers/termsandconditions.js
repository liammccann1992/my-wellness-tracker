'use strict';

/**
 * @ngdoc function
 * @name myWellnessTrackerApp.controller:TermsandconditionsCtrl
 * @description
 * # TermsandconditionsCtrl
 * Controller of the myWellnessTrackerApp
 */
angular.module('myWellnessTrackerApp')
  .controller('TermsandconditionsCtrl', function ($rootScope,$scope,permissionsService,toastr) {
    $scope.title = "Terms and Conditions";


    $scope.accept = function(){
      permissionsService.setTermsAndConditionsComplete(true);
      toastr.info("","You have agreed to My Wellness Tracker's Terms and Conditions");
      $rootScope.go("");
    }

  });
