'use strict';

/**
 * @ngdoc directive
 * @name myWellnessTrackerApp.directive:formButtons
 * @description
 * # formButtons
 */
angular.module('myWellnessTrackerApp')
  .directive('formButtons', function () {
    return {
      templateUrl: 'templates/formbuttons.html',
      restrict: 'E'
    };
  });
