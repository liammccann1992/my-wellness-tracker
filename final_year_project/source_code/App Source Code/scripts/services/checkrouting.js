'use strict';

/**
 * @ngdoc service
 * @name myWellnessTrackerApp.checkRouting
 * @description
 * # checkRouting
 * Factory in the myWellnessTrackerApp.
 */
angular.module('myWellnessTrackerApp')
  .factory('checkRouting', function ($rootScope, permissionsService) {

    return {
      check: function () {
        if (!permissionsService.termsAndConditionsComplete()) {
              $rootScope.go("termsAndConditions");
        }else if(!permissionsService.disclaimerComplete()){
          $rootScope.go("Disclaimer");
        }
      }
    };
  });
