'use strict';

/**
 * @ngdoc service
 * @name myWellnessTrackerApp.graphService
 * @description
 * # graphService
 * Service in the myWellnessTrackerApp.
 */
angular.module('myWellnessTrackerApp')
  .service('graphService', function (SideEffectService) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    var service = {};

    var colors = ["#AACEA1",
      "#AFAFC7",
      "#585981",
      "#8B8CAB",
      "#C99DB3",
      "#89B37E",
      "#985F7C",
      "#9E8D5A",
      "#6F7095",
      "#46476E",
      "#AF7B96",
      "#B9A774",
      "#D5C596",
      "#F5E7BF",
      "#824A67",
      "#DEBECE",
      "#C8E2C1",
      "#6E9B62",
      "#58854C",
      "#FFF6DA"];

    service.sideEffectsLastNDaysToPieChart = function (days) {
      var all = SideEffectService.filterLastNDays(days);
      var grouped = SideEffectService.totalSideEffects(all);

      var pieChartData = [];

      for (var i = 0; i < grouped.length; i++) {
        pieChartData.push({
          'value': grouped[i].total,
          "label": grouped[i].name,
          "color": colors[i]
        })
      }

      return pieChartData;
    };

    service.sideEffectsLastNDaysToGraph = function (days) {
      var all = SideEffectService.filterLastNDays(days);
      var grouped = SideEffectService.groupByDate(all);

      var labels = Object.keys(grouped);
      var values = [];

      for(var i = 0 ; i < labels.length;i++){
        var key = labels[i];
        var cur = grouped[key];
        values.push(cur);
      }
      return { "labels" : labels, "data" : values};
    }

    return service;


  });
