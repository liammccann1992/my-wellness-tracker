'use strict';

/**
 * @ngdoc service
 * @name myWellnessTrackerApp.pageService
 * @description
 * # pageService
 * Service in the myWellnessTrackerApp.
 */
angular.module('myWellnessTrackerApp')
  .service('pageService', function (fileLoader,$q) {
    var service = {};

    service.getPageInformation = function (pageName){
      var defer = $q.defer();

      fileLoader.getFile("pageInformation")
        .then(function (res) {
          console.log("THENTHENTHEN");
          for(var i = 0 ; i < res.length ; i++){
            if(res[i].name == pageName)
              defer.resolve(res[i]);
          }
          defer.reject("No Page found");
         ;
        }, function (err) {
          defer.reject(err);
        });

      return defer.promise;
    };

    return service;
  });
