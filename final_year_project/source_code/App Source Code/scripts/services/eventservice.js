'use strict';

/**
 * @ngdoc service
 * @name myWellnessTrackerApp.eventService
 * @description
 * # eventService
 * Service in the myWellnessTrackerApp.
 */
angular.module('myWellnessTrackerApp')
  .service('eventService', function (localStorageService,dateHelperService,$q,fileLoader,randomGenerator ) {
    var service = {};

    service.getSideEffectNameFromId = function(id){

      var deferred = $q.defer();

      fileLoader.getFile("events").then(function(events){
        for(var i = 0 ; i < events.length; i++){
          if(events[i].id == id) {
            deferred.resolve(events[i].name);
          }
        }
      });

      return deferred.promise;
    }

    service.add = function(id,event){
      var all = service.getAll();

      service.getSideEffectNameFromId(id).then(function(res){
        event.eventId = id;
        event.unixTime =
          all.push(
            {
              "id" :  randomGenerator.randomLongString(),
              "eventId": id,
              "name" : res,
              "unixTime": dateHelperService.getUnixDateCurrentDate(event.date),
              "who": event.who,
              "where": event.where,
              "purpose": event.purpose,
              "outcome": event.outcome
            }
          );
        service.setAll(all);
      });
    }

    service.edit = function(id,event){
      var all = service.getAll();

      for(var i = 0 ; i < all.length;i++){
        if(all[i].id == id){
          all[i].with = event.with;
          all[i].who = event.who;
          all[i].where = event.where;
          all[i].purpose = event.purpose;
          all[i].outcome = event.outcome;
          all[i].unixTime = dateHelperService.getUnixDateCurrentDate(event.date);

          service.setAll(all);
          return;
        }
      }
    }
    service.delete = function(id){
      console.log("ID: " + id);
      var all = service.getAll();
      console.log("#" + all.length);
      var index = -1;

      for(var i = 0 ; i < all.length;i++){
        if(all[i].id == id){
          console.log("Found At#" + i);
          index = i;
        }
      }

      if(index != -1){
        all.splice(index,1);
        service.setAll(all);
      }
      return;
    }


    service.getAll = function(){
      var all = localStorageService.get("events");

      if(all == null || all == undefined)
        return new Array();
      return all;
    };


    service.getById = function(id){
      console.log("GET BY ID: " + id);
      var all = service.getAll();

      for(var i = 0 ; i < all.length;i++){
        if(all[i].id == id){
          var obj = all[i];
          obj.date = dateHelperService.unixDateToUkDate(obj.unixTime);
        }
          return all[i];
      }

      return null;
    }

    service.setAll = function(all){
      if(all == null || all == undefined)
        all = new Array();

      localStorageService.set("events",all);
    };

    service.getAllFormatted = function(){
      var all = service.getAll();

      for(var i = 0; i < all.length;i++){
        all[i].jsDate = new Date(all[i].unixTime*1000);
        all[i].FullTextDate = dateHelperService.dateToFullTextDate(all[i].jsDate);
      }
      return all;
    };

    //Filters
    service.filterNDays = function (all,days,past){
      var currentDate = new Date();
      var dateNdaysAgo = new Date();

      if(past === undefined){
        dateNdaysAgo.setDate(dateNdaysAgo.getDate()-1);;
      }
      else if(past){
        dateNdaysAgo.setDate(dateNdaysAgo.getDate()-days);
      }else if(!past){
        dateNdaysAgo.setDate(dateNdaysAgo.getDate()+days);
      }


      var filteredList = [];
      for(var i = 0; i < all.length;i++){
        if(past === undefined){
          if(dateHelperService.isToday(all[i].jsDate)){
            filteredList.push(all[i]);
          }
        }
        else if(past){
          if(dateHelperService.inRange(all[i].jsDate,dateNdaysAgo,currentDate)){
            filteredList.push(all[i]);
          }
        }else if(!past){
          if(dateHelperService.inRange(all[i].jsDate,currentDate,dateNdaysAgo)){
            filteredList.push(all[i]);
          }
        }

      }
      return filteredList;
    };



    //service.filterLastToday = function (all){
    //  var currentDate = new Date();
    //  currentDate.setHours(0,0,0,0);
    //
    //  var filteredList = [];
    //  for(var i = 0; i < all.length;i++){
    //
    //    if(dateHelperService.compare(all[i].jsDate,currentDate) == 0){
    //      filteredList.push(all[i]);
    //    }
    //  }
    //  console.log("Last today events count = "  + filteredList.length);
    //  return filteredList;
    //};
    return service;
  });
