'use strict';

/**
 * @ngdoc service
 * @name myWellnessTrackerApp.SideEffectService
 * @description
 * # SideEffectService
 * Service in the myWellnessTrackerApp.
 */
angular.module('myWellnessTrackerApp')
  .service('SideEffectService', function (fileLoader,localStorageService,dateHelperService,$q) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    var service = {};


    service.getSideEffectNameFromId = function(id){

      var deferred = $q.defer();

      fileLoader.getFile("sideEffects").then(function(sideEffects){
        for(var i = 0 ; i < sideEffects.length; i++){
          if(sideEffects[i].id == id) {
            deferred.resolve(sideEffects[i].name);
          }
        }
      });

      return deferred.promise;
    }

    service.add = function(id,when,times){

      var all  = service.getAll();

      service.getSideEffectNameFromId(id).then(function(res){

        all.push({
          "sideEffectId" : id,
          "name" : res,
          "times" : times,
          "unixTime" : dateHelperService.getUnixDateCurrentDate(when)
        });
        service.setAll(all);
      });



    };


    service.getAll = function(){
      var all = localStorageService.get("sideEffects");

      if(all == null || all == undefined)
        return new Array();
      return all;
    };

    service.setAll = function(all){
      if(all == null || all == undefined)
        all = new Array();

      localStorageService.set("sideEffects",all);
    };

    service.getAllFormatted = function(){
      var all = service.getAll();

      for(var i = 0; i < all.length;i++){
        all[i].jsDate = new Date(all[i].unixTime*1000);
        all[i].uiDateTime = dateHelperService.dateToDateTime(all[i].jsDate);
        all[i].uiTime = dateHelperService.dateToTime(all[i].jsDate);
        all[i].uiDate = dateHelperService.dateToDate(all[i].jsDate);
        all[i].FullTextDate = dateHelperService.dateToFullTextDate(all[i].jsDate);
      }
      return all;
    };

    service.filterLastNDays = function (days){
      var all = service.getAllFormatted();

      if(days === null){
        return all;
      }
      var currentDate = new Date();
      var date60daysAgo = new Date();
      date60daysAgo.setDate(date60daysAgo.getDate()-days);

      var filteredList = [];
      for(var i = 0; i < all.length;i++){
        if(dateHelperService.inRange(all[i].jsDate,date60daysAgo,currentDate)){
          filteredList.push(all[i]);
        }
      }
      return filteredList;
    };


    /*This function will take in a list of side effects and return an object similar to below

    [
      { "sideEffectId" : 0, "name" : "sideEffectname" , "totalTimes" : 100}.
     { "sideEffectId" : 0, "name" : "sideEffectname" , "totalTimes" : 100}

    */
    service.totalSideEffects = function (all){

      var returnObj = [];
      //Declare array of side effects id which have been calculated
      var visitedIds = [];

      //For each side effect
      for(var i = 0 ; i < all.length ; i++){
        //Get the id of the element we are on
        var itemI = all[i];
        var id = itemI.sideEffectId;

        //If we have not yet calculated the totla times for this element
        //e.g. doesnt exicsit in the array list
        if(visitedIds.indexOf(id) == -1){
          var totalTimes = 0;

          //Go through the arryay totaling up the number of times
          for(var j = 0 ; j < all.length ;j++){
            var item = all[j]; //Current item

            if(item.sideEffectId == id){
              var times = all[j].times;//Number if times that day
              totalTimes += times;
            }
          }

          visitedIds.push(id);

          returnObj.push({
            "id" : id,
            "name" :itemI.name,
            "total" : totalTimes
          });
        }
      }

      return returnObj;
    }

    service.getFirstRecordedSideEffect = function(){
      var all = service.getAllFormatted();
      var firstObj = {};

      if(all.length >= 1){
        var first = new Date().getTime();
        for(var i = 0 ; i < all.length;i++){
            if(all[i].unixTime < first){
              first = all[i].unixTime;
                firstObj.name = all[i].name;
                firstObj.date = all[i].FullTextDate;
            }
        }
      }else{
        return null;
      }

      return firstObj;
    }

    service.getLastRecordedSideEffect = function(){
      var all = service.getAllFormatted();
      var lastObj = {};

      if(all.length >= 1){
        var last = 0;
        for(var i = 0 ; i < all.length;i++){
          if(all[i].unixTime > last){
            last =  all[i].unixTime;
            lastObj.name = all[i].name;
            lastObj.date = all[i].FullTextDate;
          }
        }
      }else{
        return null;
      }

      return lastObj;
    }

    function comp(a, b) {
      return new Date(a.jsDate) - new Date(b.jsDate);
    }

    function compNewestFirst(a, b) {
      return new Date(b.jsDate).getTime() - new Date(a.jsDate).getTime();
    }

    service.sortByDate = function(all,newestFirst){
      if(newestFirst) {
        all.sort(compNewestFirst);
      }else{
        all.sort(comp);
      }
      return all;
    }

    service.groupByDate = function(all){
      all.sort(comp);
      var returnObj = {};

      for(var i = 0; i < all.length;i++){
        var item = all[i];
        var date = all[i].uiDate;


        if(date in returnObj){
            returnObj[date] += item.times;
        }else{
            returnObj[date] = item.times;
        }
      }

      return returnObj;

    };

    return service;
  });
