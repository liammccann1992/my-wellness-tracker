'use strict';

/**
 * @ngdoc service
 * @name myWellnessTrackerApp.offCanvas
 * @description
 * # offCanvas
 * Factory in the myWellnessTrackerApp.
 */
angular.module('myWellnessTrackerApp')
  .factory('offCanvas', function (cnOffCanvas) {



    // Service logic
    // ...

    var meaningOfLife = 42;
    var created = {};

    // Public API here
    return {
      someMethod: function () {
        return meaningOfLife;
      },
      create : function(templateName){

        created =  cnOffCanvas({
          controller: function ($rootScope) {
            this.close = function(){

              console.log("Close");

            }

            this.go = function(path){
              console.log("Go");
              created.toggle();
              $rootScope.go(path);
            }
          },
          controllerAs : "offcanvas",
          container : '.container',
          templateUrl: 'templates/offcanvas/' + templateName  + '.html'
        });
      return created;
      }
    };
  });
